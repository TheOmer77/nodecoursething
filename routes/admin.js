const express = require('express');
const router = express.Router();

const adminController = require('../controllers/admin');
const isAuth = require('../middleware/isAuth');
const { validateProduct } = require('../middleware/validation');

router.get('/products', isAuth, adminController.getProductsPage);
router.get('/addProduct', isAuth, adminController.getAddProductPage);
router.get(
  '/editProduct/:productId',
  isAuth,
  adminController.getEditProductPage
);

router.post('/addProduct', isAuth, validateProduct, adminController.addProduct);

router.put(
  '/editProduct',
  isAuth,
  validateProduct,
  adminController.editProduct
);

router.delete('/products/:productId', isAuth, adminController.deleteProduct);

module.exports = router;
