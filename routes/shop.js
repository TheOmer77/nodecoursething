const express = require('express');
const router = express.Router();

const shopController = require('../controllers/shop');
const isAuth = require('../middleware/isAuth');

router.get('/', shopController.getIndexPage);
router.get('/products', shopController.getProductsPage);
router.get('/products/:productId', shopController.getProductDetailPage);
router.get('/cart', isAuth, shopController.getCartPage);
router.get('/orders', isAuth, shopController.getOrdersPage);
router.get('/orders/:orderId', isAuth, shopController.getInvoice);
router.get('/checkout', isAuth, shopController.getCheckoutPage);
router.get('/checkout/success', isAuth, shopController.createOrder);
router.get('/checkout/cancel', isAuth, shopController.getCheckoutPage);

router.post('/cart', isAuth, shopController.addToCart);
router.post('/createOrder', isAuth, shopController.createOrder);

router.delete('/removeFromCart', isAuth, shopController.removeFromCart);

module.exports = router;
