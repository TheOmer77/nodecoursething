const express = require('express');
const router = express.Router();

const authController = require('../controllers/auth');
const { validateLogin, validateSignup } = require('../middleware/validation');

router.get('/login', authController.getLoginPage);
router.get('/signup', authController.getSignupPage);
router.get('/resetPassword', authController.getResetPasswordPage);
router.get('/resetPassword/:token', authController.getNewPasswordPage);

router.post('/login', validateLogin, authController.login);
router.post('/logout', authController.logout);
router.post('/signup', validateSignup, authController.signUp);
router.post('/resetPassword', authController.sendResetPasswordRequest);
router.post('/newPassword', authController.updatePassword);

module.exports = router;
