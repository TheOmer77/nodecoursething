const isAuth = (req, res, next) => {
  if (!req.session.isAuthenticated) return res.redirect('/login');
  else next();
};

module.exports = isAuth;
