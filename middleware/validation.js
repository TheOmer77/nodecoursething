const { body } = require('express-validator');

const User = require('../models/user');

const messages = require('../utils/messages.json');

const validateLogin = [
  body(['email', 'password'], messages.emptyFields).notEmpty(),
  body('email', messages.emailDoesntExist).custom((value, { req }) => {
    return User.findOne({ email: value }).then((user) => {
      if (!user) {
        return Promise.reject();
      }
    });
  }),
  body('password').trim(),
];

const validateSignup = [
  body(
    ['name', 'email', 'password', 'confirmPassword'],
    messages.emptyFields
  ).notEmpty(),
  body('email')
    .isEmail()
    .withMessage(messages.invalidEmail)
    .custom((value, { req }) => {
      return User.findOne({ email: value }).then((user) => {
        if (user) {
          return Promise.reject();
        }
      });
    })
    .withMessage(messages.emailExists),
  body('password', messages.passwordTooShort)
    .isLength({
      min: 6,
    })
    .trim(),
  body('confirmPassword')
    .custom((value, { req }) => {
      if (value !== req.body.password)
        throw new Error(messages.passwordsDontMatch);
      else return true;
    })
    .trim(),
];

const validateProduct = [
  body(['title', 'price'], messages.emptyRequiredFields).notEmpty(),
  body('title').trim(),
  // body('imgUrl', messages.invalidImgUrl).isURL(),
  body('price', messages.priceZero).isFloat({ gt: 0 }),
];

module.exports = { messages, validateLogin, validateSignup, validateProduct };
