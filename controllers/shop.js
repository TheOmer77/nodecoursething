const fs = require('fs');
const path = require('path');
const PDFDocument = require('pdfkit');
const stripe = require('stripe')(
  'sk_test_51HzHQzIKzf2pGke37GrhtDnf6DP87wVJO03YGv8e0SnBapW8O0PPYToYlRbinx02OHFYOdZhNGVkA1NLuoKy2kep00WK439yV1'
);

const messages = require('../utils/messages.json');
const throwError = require('../utils/throwError');

// Models
const Product = require('../models/product');
const Order = require('../models/order');
const session = require('express-session');

const ITEMS_ON_HOMEPAGE = 3;
const ITEMS_PER_PAGE = 6;

/* GET requests */

const getIndexPage = (req, res, next) => {
  Product.find()
    .limit(ITEMS_ON_HOMEPAGE)
    .then((products) => {
      res.render('shop/index', {
        url: req.url,
        pageTitle: 'Home',
        products,
      });
    })
    .catch((err) => {
      return throwError.throw500(err, next);
    });
};

const getProductsPage = (req, res, next) => {
  const page = +req.query.page || 1; // Page 1 is the default page
  let totalProducts;

  Product.find()
    .countDocuments()
    .then((numProducts) => {
      totalProducts = numProducts;
      return Product.find()
        .skip((page - 1) * ITEMS_PER_PAGE)
        .limit(ITEMS_PER_PAGE);
    })
    .then((products) => {
      res.render('shop/productList', {
        url: req.url,
        pageTitle: 'All Products',
        products,
        totalProducts,
        page,
        hasNextPage: ITEMS_PER_PAGE * page < totalProducts,
        hasPrevPage: page > 1,
        lastPage: Math.ceil(totalProducts / ITEMS_PER_PAGE),
      });
    })
    .catch((err) => {
      return throwError.throw500(err, next);
    });
};

const getProductDetailPage = (req, res, next) => {
  const productId = req.params.productId;
  Product.findById(productId).then((product) => {
    if (product)
      res.render('shop/productDetail', {
        url: req.url,
        pageTitle: `Product Details - ${product.title}`,
        product,
      });
  });
};

const getCartPage = (req, res, next) => {
  req.user
    .populate('cart.items.productId')
    .execPopulate()
    .then((user) => {
      const cartProducts = user.cart.items;
      let totalPrice = 0;
      cartProducts.forEach((product) => {
        totalPrice += product.productId.price * product.quantity;
      });
      totalPrice = Math.round((totalPrice + Number.EPSILON) * 100) / 100;
      res.render('shop/cart', {
        url: req.url,
        pageTitle: 'Cart',
        cartProducts,
        totalPrice,
      });
    })
    .catch((err) => {
      return throwError.throw500(err, next);
    });
};

const getOrdersPage = (req, res, next) => {
  Order.find()
    .then((orders) => {
      const orderTotals = [];
      orders.forEach((order) => {
        let totalPrice = 0;
        order.products.forEach((p) => {
          totalPrice += p.product.price * p.quantity;
        });
        orderTotals.push(Math.round((totalPrice + Number.EPSILON) * 100) / 100);
      });
      res.render('shop/orders', {
        url: req.url,
        pageTitle: 'Orders',
        orders,
        orderTotals,
      });
    })
    .catch((err) => {
      return throwError.throw500(err, next);
    });
};

const getInvoice = (req, res, next) => {
  const orderId = req.params.orderId;
  Order.findById(orderId)
    .then((order) => {
      if (!order) return next(new Error(messages.orderNotFound));
      else if (order.user.toString() !== req.user._id.toString())
        return next(new Error(messages.unauthorized));
      else {
        const invoiceName = `invoice-${orderId}.pdf`;
        const invoicePath = path.join('data', 'invoices', invoiceName);

        const doc = new PDFDocument();

        let totalPrice = 0;

        doc.pipe(fs.createWriteStream(invoicePath));
        doc.pipe(res);
        res.setHeader('Content-Type', 'application/pdf');
        res.setHeader(
          'Content-Disposition',
          `inline; filename="${invoiceName}"`
        );

        // Register doc fonts
        doc.registerFont('heading', 'fonts/Roboto_Slab/RobotoSlab-Bold.ttf');
        doc.registerFont('text-regular', 'fonts/Roboto/Roboto-Regular.ttf');
        doc.registerFont('text-bold', 'fonts/Roboto/Roboto-Bold.ttf');

        // Text colors
        const textColors = {
          h1: '#1a1a1a',
          h2: '#4d4d4d',
          text: '#1a1a1a',
        };

        // Write doc
        doc
          .font('heading')
          .fontSize(32)
          .fillColor(textColors.h1)
          .text('Invoice');
        doc.fontSize(14).moveDown();
        order.products.forEach((p) => {
          totalPrice += p.product.price * p.quantity;
          doc
            .font('heading')
            .fontSize(18)
            .fillColor(textColors.h2)
            .text(`${p.product.title} × ${p.quantity}`);
          doc
            .font('text-regular')
            .fontSize(14)
            .fillColor(textColors.text)
            .text(`$${p.product.price} / unit`);
          doc
            .font('text-regular')
            .fontSize(14)
            .text(`Total: $${p.product.price * p.quantity}`);
          doc.moveDown();
        });
        totalPrice = Math.round((totalPrice + Number.EPSILON) * 100) / 100;
        doc
          .font('text-bold')
          .fontSize(14)
          .fillColor(textColors.text)
          .text('Total price: ', { continued: true })
          .font('text-regular')
          .text(`$${totalPrice}`);

        doc.end();
      }
    })
    .catch((err) => throwError.throw500(err, next));
};

const getCheckoutPage = (req, res, next) => {
  let cartProducts, totalPrice;
  req.user
    .populate('cart.items.productId')
    .execPopulate()
    .then((user) => {
      cartProducts = user.cart.items;
      totalPrice = 0;
      cartProducts.forEach((product) => {
        totalPrice += product.productId.price * product.quantity;
      });
      totalPrice = Math.round((totalPrice + Number.EPSILON) * 100) / 100;

      return stripe.checkout.sessions.create({
        payment_method_types: ['card'],
        line_items: cartProducts.map((p) => {
          return {
            name: p.productId.title,
            description: p.productId.description,
            // amount: p.productId.price * 100,
            amount: Math.round((p.productId.price + Number.EPSILON) * 100),
            currency: 'usd',
            quantity: p.quantity,
          };
        }),
        success_url: `${req.protocol}://${req.get('host')}/checkout/success`,
        cancel_url: `${req.protocol}://${req.get('host')}/checkout/cancel`,
      });
    })
    .then((session) => {
      res.render('shop/checkout', {
        url: req.url,
        pageTitle: 'Checkout',
        cartProducts,
        totalPrice,
        sessionId: session.id,
      });
    })
    .catch((err) => {
      return throwError.throw500(err, next);
    });
};

/* Other requests */

const addToCart = (req, res, next) => {
  const productId = req.body.productId;
  req.user
    .addToCart(productId)
    .then(() => res.redirect('/cart'))
    .catch((err) => {
      return throwError.throw500(err, next);
    });
};

const removeFromCart = (req, res, next) => {
  const productId = req.body.productId;
  req.user
    .removeFromCart(productId)
    .then(() => {
      res.redirect('/cart');
    })
    .catch((err) => {
      return throwError.throw500(err, next);
    });
};

const createOrder = (req, res, next) => {
  req.user
    .populate('cart.items.productId')
    .execPopulate()
    .then((user) => {
      const products = user.cart.items.map((item) => {
        return { quantity: item.quantity, product: { ...item.productId._doc } };
      });
      const newOrder = new Order({
        products,
        user: req.user._id,
      });
      return newOrder.save();
    })
    .then(() => req.user.clearCart())
    .then(() => res.redirect('/orders'))
    .catch((err) => {
      return throwError.throw500(err, next);
    });
};

module.exports = {
  getIndexPage,
  getProductsPage,
  getProductDetailPage,
  getCartPage,
  getOrdersPage,
  getInvoice,
  getCheckoutPage,
  addToCart,
  removeFromCart,
  createOrder,
};
