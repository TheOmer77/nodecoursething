const { validationResult } = require('express-validator');
const crypto = require('crypto');
const bcrypt = require('bcryptjs');
const nodemailer = require('nodemailer');
const sendgridTransport = require('nodemailer-sendgrid-transport');

const messages = require('../utils/messages.json');
const throwError = require('../utils/throwError');

// Init nodemailer
const transporter = nodemailer.createTransport(
  sendgridTransport({
    auth: {
      api_key:
        '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',
    },
  })
);

// GET requests

const User = require('../models/user');

const getLoginPage = (req, res, next) => {
  res.render('auth/login', {
    url: req.url,
    pageTitle: 'Login',
  });
};

const getSignupPage = (req, res, next) => {
  res.render('auth/signup', {
    url: req.url,
    pageTitle: 'Sign up',
  });
};

const getResetPasswordPage = (req, res, next) => {
  res.render('auth/resetPassword', {
    url: req.url,
    pageTitle: 'Reset Password',
  });
};

const getNewPasswordPage = (req, res, next) => {
  const { token } = req.params;
  User.findOne({ resetToken: token, resetTokenExpiration: { $gt: Date.now() } })
    .then((user) => {
      res.render('auth/newPassword', {
        url: req.url,
        pageTitle: 'Create a new password',
        userId: user._id.toString(),
        passwordToken: token,
      });
    })
    .catch((err) => {
      return throwError.throw500(err, next);
    });
};

// Other requests

const login = (req, res, next) => {
  const { email, password } = req.body;
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(422).render('auth/login', {
      url: req.url,
      pageTitle: 'Login',
      errorMsg: errors.array()[0].msg,
      validationErrors: errors.array(),
      prevInput: { email, password },
    });
  } else {
    User.findOne({ email })
      .then((user) => {
        bcrypt
          .compare(password, user.password)
          .then((match) => {
            if (match) {
              req.session.isAuthenticated = true;
              req.session.user = user;
              req.session.save((err) => {
                if (err) console.error(err);
                res.redirect('/');
              });
            } else {
              const validationErrors = [
                {
                  value: password,
                  msg: messages.passwordIncorrect,
                  param: 'password',
                  location: 'body',
                },
              ];
              return res.status(422).render('auth/login', {
                url: req.url,
                pageTitle: 'Login',
                errorMsg: validationErrors[0].msg,
                validationErrors,
                prevInput: { email, password },
              });
            }
          })
          .catch((err) => {
            console.error(err);
            res.redirect('/login');
          });
      })
      .catch((err) => {
        return throwError.throw500(err, next);
      });
  }
};

const logout = (req, res, next) => {
  req.session.destroy((err) => {
    if (err) console.error(err);
    res.redirect('/');
  });
};

const signUp = (req, res, next) => {
  const { name, email, password, confirmPassword } = req.body;
  const errors = validationResult(req);

  if (!errors.isEmpty()) {
    return res.status(422).render('auth/signup', {
      url: req.url,
      pageTitle: 'Sign up',
      errorMsg: errors.array()[0].msg,
      validationErrors: errors.array(),
      prevInput: { name, email, password, confirmPassword },
    });
  } else {
    bcrypt
      .hash(password, 12)
      .then((hashedPassword) => {
        const newUser = new User({
          name,
          email,
          password: hashedPassword,
          cart: { items: [] },
        });
        return newUser.save();
      })
      .then(() => {
        req.flash('successMsg', 'Account created successfully.');
        res.redirect('/login');

        // This doesn't work :(
        return transporter.sendMail({
          to: email,
          from: 'shop@nodeThing.com',
          subject: 'Successfully signed up to NodeThing!',
          html:
            '<h1>Successfully signed up</h1><p>Thanks for signing up to this stupid thing! You may now delete this useless email.</p>',
        });
      })
      .catch((err) => {
        return throwError.throw500(err, next);
      });
  }
};

const sendResetPasswordRequest = (req, res, next) => {
  let token;
  crypto.randomBytes(32, (err, buffer) => {
    if (err) {
      console.error(err);
      req.flash('errorMsg', 'An error has occurred.');
      return res.redirect('/resetPassword');
    }
    token = buffer.toString('hex');
  });
  User.findOne({ email: req.body.email })
    .then((user) => {
      if (!user) {
        req.flash('errorMsg', messages.emailDoesntExist);
        return res.redirect('/resetPassword');
      }
      user.resetToken = token;
      user.resetTokenExpiration = Date.now() + 3600000; // Current date + one hour
      user.save().then(() => {
        console.log(`
        Password reset link for ${user.name} (email: ${user.email}):
        http://localhost:3000/resetPassword/${token}
        Note: this is only valid for one hour.
        `);
        req.flash(
          'errorMsg',
          'This is where you would be sent an email to reset you password. However, SendGrid hates me - and as such the content of the email was instead printed to the console.'
        );
        return res.redirect('/login');
      });
    })
    .catch((err) => {
      return throwError.throw500(err, next);
    });
};

const updatePassword = (req, res, next) => {
  const { newPassword, confirmNewPassword, userId, passwordToken } = req.body;
  let resetUser;
  if (newPassword.length <= 0 || confirmNewPassword.length <= 0) {
    req.flash('errorMsg', 'Please fill in all fields.');
    res.redirect(`/resetPassword/${passwordToken}`);
  } else if (newPassword !== confirmNewPassword) {
    req.flash('errorMsg', 'Passwords do not match.');
    res.redirect(`/resetPassword/${passwordToken}`);
  } else {
    User.findOne({
      resetToken: passwordToken,
      resetTokenExpiration: { $gt: Date.now() },
      _id: userId,
    })
      .then((user) => {
        resetUser = user;
        return bcrypt.hash(newPassword, 12);
      })
      .then((hashedPassword) => {
        resetUser.password = hashedPassword;
        resetUser.resetToken = undefined;
        resetUser.resetTokenExpiration = undefined;
        return resetUser.save();
      })
      .then(() => {
        req.flash('successMsg', 'Password has been updated.');
        res.redirect('/login');
      })
      .catch((err) => {
        return throwError.throw500(err, next);
      });
  }
};

module.exports = {
  getLoginPage,
  getSignupPage,
  getResetPasswordPage,
  getNewPasswordPage,
  login,
  logout,
  signUp,
  sendResetPasswordRequest,
  updatePassword,
};
