const get404 = (req, res, next) => {
  res.status(404).render('errors/404', {
    url: req.url,
    pageTitle: '404 - Page not found',
  });
};

const get500 = (req, res, next) => {
  res.status(500).render('errors/500', {
    url: req.url,
    pageTitle: '500 - Internal server error',
  });
};

module.exports = { get404, get500 };
