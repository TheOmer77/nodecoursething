const { validationResult } = require('express-validator');

const messages = require('../utils/messages.json');
const throwError = require('../utils/throwError');
const { deleteFile } = require('../utils/files');

// Models
const Product = require('../models/product');

const ADMIN_PREFIX = '/admin';

/* GET requests */

const getProductsPage = (req, res, next) => {
  Product.find({ userId: req.user._id })
    .then((products) => {
      res.render('admin/productList', {
        url: ADMIN_PREFIX + req.url,
        pageTitle: 'Admin - Manage Products',
        products,
      });
    })
    .catch((err) => {
      return throwError.throw500(err, next);
    });
};

const getAddProductPage = (req, res, next) => {
  res.render('admin/editproduct', {
    url: ADMIN_PREFIX + req.url,
    pageTitle: 'Admin - Add Product',
    editing: false,
    product: {},
  });
};

const getEditProductPage = (req, res, next) => {
  const editMode = req.query.edit;
  if (editMode != 'true') {
    req.flash(messages.productNotFound);
    return res.redirect('/admin/products');
  }
  const productId = req.params.productId;
  Product.findById(productId)
    .then((product) => {
      if (!product) {
        req.flash('errorMsg', messages.productNotFound);
        return res.redirect('/admin/products');
      } else if (product.userId.toString() !== req.user._id.toString()) {
        req.flash('errorMsg', messages.productEditingNotAllowed);
        res.redirect('/admin/products');
      } else {
        res.render('admin/editproduct', {
          url: ADMIN_PREFIX + req.url,
          pageTitle: 'Admin - Edit Product',
          editing: editMode,
          product,
        });
      }
    })
    .catch((err) => {
      return throwError.throw500(err, next);
    });
};

/* Other requests */

const addProduct = (req, res, next) => {
  const { title, price, description, imgUrlType, imgUrl } = req.body;
  const img = req.file;
  const errors = validationResult(req);

  const rerenderPage = (
    statusCode,
    errorMsg = errors.array()[0].msg,
    validationErrors = errors.array()
  ) =>
    res.status(statusCode).render('admin/editproduct', {
      url: ADMIN_PREFIX + req.url,
      pageTitle: 'Admin - Add Product',
      editing: false,
      product: { title, price, description },
      errorMsg,
      hasError: true,
      validationErrors,
    });

  if (!img && imgUrlType === 'upload')
    return rerenderPage(422, messages.fileNotImage, [
      {
        value: img,
        msg: messages.fileNotImage,
        param: 'img',
        location: 'body',
      },
    ]);

  if (!errors.isEmpty()) {
    return rerenderPage(422);
  } else {
    const imageUrl = imgUrlType === 'upload' ? `/${img.path}` : imgUrl;

    const product = new Product({
      title,
      price,
      description,
      imgUrlType,
      imgUrl: imageUrl,
      userId: req.user,
    });

    product
      .save()
      .then(() => {
        res.redirect('/admin/products');
      })
      .catch((err) => {
        return throwError.throw500(err, next);
      });
  }
};

const editProduct = (req, res, next) => {
  const { productId, title, price, description, imgUrlType, imgUrl } = req.body;
  const img = req.file;
  const errors = validationResult(req);

  Product.findById(productId)
    .then((product) => {
      if (product.userId.toString() !== req.user._id.toString()) {
        req.flash('errorMsg', messages.productEditingNotAllowed);
      } else {
        const prevUrlType = product.imgUrlType;
        const prevImgUrl = product.imgUrl;

        product.title = title;
        product.price = price;
        product.description = description;
        product.imgUrlType = imgUrlType;
        if (img && imgUrlType === 'upload') product.imgUrl = `/${img.path}`;
        else if (imgUrl.length > 0) product.imgUrl = imgUrl;
        if (prevUrlType === 'upload' && product.imgUrl !== prevImgUrl)
          deleteFile(prevImgUrl.slice(1));

        if (!errors.isEmpty()) {
          return res.status(422).render('admin/editproduct', {
            url: ADMIN_PREFIX + req.url,
            pageTitle: 'Admin - Edit Product',
            editing: true,
            product,
            errorMsg: errors.array()[0].msg,
            hasError: true,
            validationErrors: errors.array(),
          });
        } else {
          product.save().then(() => {
            res.redirect('/admin/products');
          });
        }
      }
    })
    .catch((err) => {
      return throwError.throw500(err, next);
    });
};

const deleteProduct = (req, res, next) => {
  const productId = req.params.productId;
  Product.findById(productId)
    .then((product) => {
      if (!product) return req.flash('errorMsg', messages.productNotFound);
      else {
        if (product.imgUrlType === 'upload')
          deleteFile(product.imgUrl.slice(1));
        return Product.deleteOne({ _id: productId, userId: req.user._id });
      }
    })
    .then(() =>
      res.status(200).json({ successMsg: messages.productDeletionSuccessful })
    )
    .catch((err) =>
      res.status(500).json({ errorMsg: messages.productDeletionFailed })
    );
};

module.exports = {
  getProductsPage,
  getAddProductPage,
  getEditProductPage,
  addProduct,
  editProduct,
  deleteProduct,
};
