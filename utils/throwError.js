const throw500 = (err = 'Internal error', next) => {
  const error = new Error(err);
  error.httpStatusCode = 500;
  return next(error);
};

module.exports = { throw500 };
