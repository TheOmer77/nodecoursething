const navItems = (isAuthenticated) => [
  {
    title: 'Home',
    path: '/',
    display: true,
  },
  {
    title: 'Products',
    path: '/products',
    display: true,
  },
  {
    title: 'Cart',
    path: '/cart',
    display: isAuthenticated,
  },
  {
    title: 'Orders',
    path: '/orders',
    display: isAuthenticated,
  },
  {
    title: 'Manage Products',
    path: '/admin/products',
    display: isAuthenticated,
  },
  {
    title: 'Login',
    path: '/login',
    display: !isAuthenticated,
    position: 'end',
  },
  {
    title: 'Sign up',
    path: '/signup',
    display: !isAuthenticated,
    position: 'end',
  },
];

module.exports = navItems;
