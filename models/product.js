const { Schema, model } = require('mongoose');

const productSchema = new Schema({
  title: { type: String, required: true },
  price: { type: Number, required: true },
  imgUrlType: { type: String, required: true, default: 'upload' },
  imgUrl: { type: String, required: true },
  description: String,
  userId: { type: Schema.Types.ObjectId, ref: 'User', required: true },
});

const productModel = model('Product', productSchema);

module.exports = productModel;
