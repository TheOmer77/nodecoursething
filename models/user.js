const { Schema, model } = require('mongoose');

const Product = require('./product');
const Order = require('./order');

const userSchema = new Schema({
  name: { type: String, required: true },
  email: { type: String, required: true },
  password: { type: String, required: true },
  resetToken: String,
  resetTokenExpiration: Date,
  cart: {
    items: {
      type: [
        {
          productId: {
            type: Schema.Types.ObjectId,
            required: true,
            ref: 'Product',
          },
          quantity: { type: Number, required: true },
        },
      ],
      required: true,
    },
  },
});

userSchema.methods.addToCart = function (productId) {
  const cartProductIndex = this.cart.items.findIndex(
    (cp) => cp.productId.toString() === productId.toString()
  );
  let newQuantity = 1;
  const updatedCartItems = [...this.cart.items];

  if (cartProductIndex >= 0) {
    newQuantity = this.cart.items[cartProductIndex].quantity + 1;
    updatedCartItems[cartProductIndex].quantity = newQuantity;
  } else {
    updatedCartItems.push({ productId, quantity: newQuantity });
  }
  this.cart = { items: updatedCartItems };
  return this.save();
};

userSchema.methods.removeFromCart = function (productId) {
  const updatedCartItems = this.cart.items.filter(
    (item) => item.productId.toString() !== productId.toString()
  );
  this.cart.items = updatedCartItems;
  return this.save();
};

userSchema.methods.clearCart = function () {
  this.cart = { items: [] };
  return this.save();
};

const userModel = model('User', userSchema);

module.exports = userModel;
