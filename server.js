const path = require('path');
const express = require('express');
const methodOverride = require('method-override');
const mongoose = require('mongoose');
const session = require('express-session');
const mongoStore = require('connect-mongodb-session')(session);
const csrf = require('csurf');
const flash = require('connect-flash');
const multer = require('multer');

const getDateString = require('./utils/getDateString');

// Models
const User = require('./models/user');

// Routes and error controller
const adminRoutes = require('./routes/admin');
const shopRoutes = require('./routes/shop');
const authRoutes = require('./routes/auth');

const errorController = require('./controllers/errors');

// Nav items for all pages
const navItems = require('./utils/navItems');

const PORT = 3000;
const MONGO_URI =
  'mongodb+srv://OmerChameleon:chameleon@cluster0.scd9o.mongodb.net/nodeThing?retryWrites=true&w=majority';
const SECRET = 'veryGoodSecretSeriouslyItIsVeryGood';

// Express app
const app = express();
app.set('view engine', 'ejs');

// Initialize stuff
const store = new mongoStore({
  uri: MONGO_URI,
  collection: 'sessions',
});
const csrfProtection = csrf();

const fileStorage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, 'images');
  },
  filename: (req, file, cb) => {
    cb(null, `${getDateString(new Date())}-${file.originalname}`);
  },
});
const fileFilter = (req, file, cb) => {
  if (
    file.mimetype === 'image/png' ||
    file.mimetype === 'image/jpg' ||
    file.mimetype === 'image/jpeg'
  ) {
    cb(null, true);
  } else {
    cb(null, false);
  }
};

// Middelware - serve static files, routes, method override, csrf, flash
app.use(express.urlencoded({ extended: false }));
app.use(multer({ storage: fileStorage, fileFilter: fileFilter }).single('img'));
app.use(express.static(path.join(__dirname, 'public')));
app.use('/images', express.static(path.join(__dirname, 'images')));
app.use(methodOverride('_method'));
app.use(
  session({
    secret: SECRET,
    resave: false,
    saveUninitialized: false,
    store,
  })
);
app.use(csrfProtection);
app.use(flash());

// Find the current user
app.use((req, res, next) => {
  if (!req.session.user) return next();
  User.findById(req.session.user._id)
    .then((user) => {
      if (!user) {
        return next();
      }
      req.user = user;
      next();
    })
    .catch((err) => {
      next(new Error(err));
    });
});

// Setup locals for all pages
app.use((req, res, next) => {
  res.locals.navItems = navItems(req.session.isAuthenticated);
  res.locals.isAuthenticated = req.session.isAuthenticated;
  res.locals.csrfToken = req.csrfToken();
  res.locals.successMsg = req.flash('successMsg');
  res.locals.errorMsg = req.flash('errorMsg');
  res.locals.validationErrors = [];
  res.locals.hasError = false;
  next();
});

// Setup routes
app.use('/admin', adminRoutes);
app.use(shopRoutes);
app.use(authRoutes);

app.get('/500', errorController.get500);
app.use(errorController.get404);

// Error handler
app.use((error, req, res, next) => {
  console.log('\x1b[31m', error, '\x1b[0m');
  errorController.get500(req, res, next);
});

// Connect to DB, and once connected start the server
mongoose
  .connect(MONGO_URI, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then(() => {
    return User.findOne();
  })
  .then(() => {
    app.listen(PORT, () => {
      console.clear();
      process.stdout.write('\033c');
      console.log(
        `\x1b[42m\x1b[30mServer is listening on port ${PORT}.\x1b[0m`
      );
    });
  })
  .catch((err) => console.error(err));
