const backdrop = document.querySelector('.backdrop');
const sideDrawer = document.querySelector('.mobile-nav');
const menuToggle = document.querySelector('#side-menu-toggle');

const backdropClickHandler = () => {
  backdrop.style.display = 'none';
  backdrop.classList.remove('fadeIn');
  backdrop.classList.add('fadeOut');
  sideDrawer.classList.remove('open');
};

const menuToggleClickHandler = () => {
  backdrop.style.display = 'block';
  backdrop.classList.remove('fadeOut');
  backdrop.classList.add('fadeIn');
  sideDrawer.classList.add('open');
};

backdrop.addEventListener('click', backdropClickHandler);
menuToggle.addEventListener('click', menuToggleClickHandler);
