const deleteProduct = (btn) => {
  const productId = btn.parentNode.querySelector('[name=productId').value;
  const csrf = btn.parentNode.querySelector('[name=_csrf').value;

  const productElement = btn.closest('article');

  fetch(`/admin/products/${productId}`, {
    method: 'DELETE',
    headers: {
      'csrf-token': csrf,
    },
  })
    .then((result) => result.json())
    .then((json) => {
      if (json.successMsg)
        productElement.parentNode.removeChild(productElement);
    })
    .catch((err) => console.error(err));
};
