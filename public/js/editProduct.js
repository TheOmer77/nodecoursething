const uploadRadio = document.getElementById('imgUploadRadio');
const urlRadio = document.getElementById('imgUrlRadio');

const imageUploadFormControl = document.getElementById(
  'img-upload-form-control'
);
const imageUrlFormControl = document.getElementById('img-url-form-control');

const onRadioChanged = (e) => {
  const value = e.target.value;
  switch (value) {
    case 'upload':
      imageUploadFormControl.style.display = 'block';
      imageUrlFormControl.style.display = 'none';
      break;
    case 'url':
      imageUploadFormControl.style.display = 'none';
      imageUrlFormControl.style.display = 'block';
      break;
  }
};

uploadRadio.addEventListener('change', onRadioChanged);
urlRadio.addEventListener('change', onRadioChanged);

/*
  Custom file upload field
  Code copied from https://tympanus.net/codrops/2015/09/15/styling-customizing-file-inputs-smart-way/
  And modified by me.
*/

const inputs = document.querySelectorAll('#img');
Array.prototype.forEach.call(inputs, function (input) {
  const label = input.nextElementSibling;
  const labelVal = label.innerHTML;

  input.addEventListener('change', function (e) {
    let fileName = '';
    if (this.files && this.files.length > 1)
      fileName = (this.getAttribute('data-multiple-caption') || '').replace(
        '{count}',
        this.files.length
      );
    else fileName = e.target.value.split('\\').pop();

    if (fileName) label.querySelector('span').innerHTML = fileName;
    else label.innerHTML = labelVal;
    imageUploadFormControl.classList.remove('form-control-invalid');
  });
});
